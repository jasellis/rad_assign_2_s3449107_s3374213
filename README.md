# Course Application

This is a course management application created for Rapid Application Development assignment 2.

## Heroku link

[https://yourcourseapp.herokuapp.com/](https://yourcourseapp.herokuapp.com/)

## Heroku log

[Click here](/heroku.txt) to view the most recent Heroku deployment log.

## Student information

- Jasmine Ellis `s3449107`
- Haotian Xu `s3374213`

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```
Install ImageMagick:

Linux
```
sudo apt-get update
sudo apt-get install imagemagick --fix-missing
```

MacOS
```
brew install imagemagick
```

Next, migrate and seed the database:

```
$ rails db:migrate
$ rails db:seed
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```